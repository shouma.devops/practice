package com.shouma.exposeapi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class HttpRequestTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getShouldReturnAllTodos() throws Exception {
        this.mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()").value(12));
    }

    @Test
    public void postWithoutIdShouldBeCreate() throws Exception {
        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"title\": \"Lombok\", \"completed\": true, \"userId\": 2}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Lombok"))
                .andExpect(jsonPath("$.completed").value(true))
                .andExpect(jsonPath("$.userId").value(2));
    }

}
