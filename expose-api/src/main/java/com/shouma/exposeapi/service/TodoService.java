package com.shouma.exposeapi.service;

import com.shouma.exposeapi.model.Todo;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TodoService implements ITodoService{

    private final AtomicLong counter = new AtomicLong();
    private final ArrayList<Todo> todos = new ArrayList<Todo>(
            Arrays.asList(
                    new Todo(counter.incrementAndGet(), "Mockito", true, 1L),
                    new Todo(counter.incrementAndGet(), "Oracle", false, 1L),
                    new Todo(counter.incrementAndGet(), "StringBuilder", true, 1L),
                    new Todo(counter.incrementAndGet(), "Collections, map et arrays", true, 2L),
                    new Todo(counter.incrementAndGet(), "Modèle OSI", false, 2L),
                    new Todo(counter.incrementAndGet(), "Gitflow", false, 2L),
                    new Todo(counter.incrementAndGet(), "Postman", true, 3L),
                    new Todo(counter.incrementAndGet(), "Jira", false, 3L),
                    new Todo(counter.incrementAndGet(), "WebClient", true, 4L),
                    new Todo(counter.incrementAndGet(), "Api Getway", false, 4L),
                    new Todo(counter.incrementAndGet(), "Api RESTful", true, 4L),
                    new Todo(counter.incrementAndGet(), "Api SOAP", false, 4L)
            )
    );
    @Override
    public ArrayList<Todo> findAll() {
        return todos;
    }

    @Override
    public Todo findById(Long id) {
        Todo todo = findTodo(id);
        if(todo == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo not found");
        } else {
            return todo;
        }
    }

    private Todo findTodo(Long id){
        return todos.stream()
                .filter(todo -> id.equals(todo.getId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Todo create(Todo todo) {
        Long newId = todos.get(todos.size() - 1).getId() + 1;
        Todo newTodo = new Todo(newId, todo.getTitle(), todo.getCompleted(), todo.getUserId());
        todos.add(newTodo);
        return newTodo;
    }

    @Override
    public Todo update(Long id, Boolean completed) {
        Todo newTodo = findTodo(id);
        if (newTodo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo Not Found");
        } else {
            delete(id);
            newTodo.setCompleted(completed);
            todos.add(newTodo);
            return newTodo;
        }
    }

    @Override
    public void delete(Long id) {
        Todo todo = findTodo(id);
        if (todo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo Not Found");
        } else {
            todos.remove(todo);
        }
    }

}
