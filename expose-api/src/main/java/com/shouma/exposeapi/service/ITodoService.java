package com.shouma.exposeapi.service;

import com.shouma.exposeapi.model.Todo;

import java.util.ArrayList;

public interface ITodoService {
    ArrayList<Todo> findAll();

    Todo findById(Long id);

    Todo create(Todo todo);

    Todo update(Long id, Boolean completed);

    void delete(Long id);
}
