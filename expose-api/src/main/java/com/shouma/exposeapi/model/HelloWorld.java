package com.shouma.exposeapi.model;

public record HelloWorld(long id, String content) {
}
