package com.shouma.exposeapi.controller;

import com.shouma.exposeapi.model.Todo;
import com.shouma.exposeapi.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
public class TodoController {

    @Autowired
    private TodoService todoService;

    // Get all todoTasks
    @GetMapping("/todos")
    @ResponseStatus(HttpStatus.OK)
    public ArrayList<Todo> getTodos() {
        return todoService.findAll();
    }


    // Get an individual todoTask
    @GetMapping("/todos/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Todo getTodo(@PathVariable("id") Long id){
        return todoService.findById(id);
    }

    // Creating a new todoTask
    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo createTodo(@RequestBody Todo todo) {
        return todoService.create(todo);
    }

    // Update an existing todoTask
//    @PutMapping("/todos/{id}")
//    public Todo updateTodo(@PathVariable("id") Long id, @RequestBody Map<String, String> req){
//        Todo todo = findTodo(id);
//        if(todo == null){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo not found");
//        } else {
//            for (String k : req.keySet()) {
//                if (k.equals("completed")) {
//                   todo.setCompleted(Boolean.parseBoolean(req.get(k)));
//                }
//            }
//        }
//        return todo;
//    }

    // Deleting a todoTask
    @DeleteMapping("/todos/{id}")
//    @ResponseStatus(HttpStatus.OK)
    public void deleteTodo(@PathVariable("id") Long id) {
        todoService.delete(id);
    }

}
