package com.shouma.consumeapi;

import com.shouma.consumeapi.model.Todo;
import com.shouma.consumeapi.service.TodoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ConsumeApiApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(ConsumeApiApplication.class, args);
		TodoService webClientService = applicationContext.getBean(TodoService.class);

		Todo todo = new Todo(15L, "Gitlab", false, 1L);

		webClientService.findById(1);
		webClientService.findAll();
		webClientService.create(todo);


	}
}
