package com.shouma.consumeapi.model;


import lombok.*;
import lombok.experimental.FieldDefaults;
import java.io.Serializable;

@FieldDefaults(level= AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Todo implements Serializable {
    @Getter @Setter
    Long id;
    @Getter @Setter
    String title;
    @Getter @Setter
    Boolean completed;
    @Getter @Setter
    Long userId;

}
