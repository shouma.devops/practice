package com.shouma.consumeapi.service;

import com.shouma.consumeapi.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class TodoService implements ITodoService {

    @Autowired
    private WebClient webClient;

    // GET /todos/{id}
    @Override
    public Mono<Todo> findById(Integer id) {
        return webClient.get()
                .uri("/todos/" + id)
                .retrieve()
                .bodyToMono(Todo.class);
    }

    // GET /todos
    @Override
    public Flux<Todo> findAll()
    {
        return webClient.get()
                /*.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)*/
                .uri("/todos")
                .retrieve()
                .bodyToFlux(Todo.class)
                .timeout(Duration.ofMillis(10_000));

    }

    // POST /todos
    @Override
    public Mono<Todo> create(Todo todo) {
        return webClient.post()
                .uri("/todos")
                .body(Mono.just(todo), Todo.class)
                .retrieve()
                .bodyToMono(Todo.class)
                .timeout(Duration.ofMillis(10_000));
    }

    // PUT /todos/{id}
    @Override
    public Mono<Todo> update(Todo todo) {
        return webClient.put()
                .uri("/todos/" + todo.getId())
                .body(Mono.just(todo), Todo.class)
                .retrieve()
                .bodyToMono(Todo.class);
    }

    // DELETE /todos/{id}
    @Override
    public Mono<Void> delete(Integer id) {
        return webClient.delete()
                .uri("/todos/" +id)
                .retrieve()
                .bodyToMono(Void.class);
    }

}