package com.shouma.consumeapi.service;

import com.shouma.consumeapi.model.Todo;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ITodoService {

    Flux<Todo> findAll();

    Mono<Todo> findById(Integer id);

    Mono<Todo> create(Todo todo);

    Mono<Todo> update(Todo todo);

    Mono<Void> delete(Integer id);

}
